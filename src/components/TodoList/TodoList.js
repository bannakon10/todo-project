import {useEffect} from 'react';
import {Collapse, Checkbox, Button} from 'antd';


const TodoList = (props) => {

    useEffect(() => {
    }, [props.item])
    const {Panel} = Collapse;
    return (
        <>

            <Collapse accordion expandIconPosition="right">
                {props.item.map((todo, index) => {
                    return (
                        <Panel header={
                            <div>
                                <Checkbox> # </Checkbox>{todo.task}
                            </div>
                        } key={index}>
                            <p>{todo.note}</p>

                            <div style={{textAlign: 'end'}}>
                                <Button type="link" style={{color: '#1E857A'}}>Update</Button>
                                <Button type="link" style={{color: 'red'}}
                                        onClick={() => {
                                            props.onDeleted && props.onDeleted(todo.id)  // bug ตอนแรกใช้ index ลบ
                                        }}>Delete</Button>
                            </div>
                        </Panel>
                    )
                })
                }

            </Collapse>


        </>

    )
}

export default TodoList;