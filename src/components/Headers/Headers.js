import {Header} from 'antd/es/layout/layout';
import './Headers.css';
import 'antd/dist/antd.css'

const Headers = () => {
    return (
        <>
            <Header style={{color: 'white', backgroundColor: '#1E857A', fontsize: 'medium'}}>Todo List Project - วางแผนกิจกรรมของคุณ</Header>
        </>
    )
}

export default Headers;