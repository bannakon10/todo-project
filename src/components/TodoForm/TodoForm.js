import React, {useEffect, useState} from 'react';
import {Card, Form, Input, Button, Row, Col} from 'antd';
import TextArea from 'antd/es/input/TextArea';
import TodoList from '../TodoList/TodoList';
import axios from "axios";

const TodoForm = () => {
    const [todos, setTodos] = useState([]);

    useEffect(() => {
        getTodo();
    }, []);

    const saveTodo = (values) => {
        addTodo(values);
    }

    const getTodo = () => {
        axios.get('https://602dd56696eaad00176dcd53.mockapi.io/api/v1/to-do')
            .then(function (response) {
                setTodos(response.data);
            })
            .catch(function (error) {
            })
    }
    const addTodo = (values) => {
        axios.post('https://602dd56696eaad00176dcd53.mockapi.io/api/v1/to-do', {
            task: values.task,
            note: values.note
        })
            .then(function (response) {
                getTodo();
                console.log(response)
            })
            .catch(function (error) {
                console.log(error)
            });
    }
    const handleDelete = (id) => {
        deleteTodo(id);
    }
    const deleteTodo = (id) => {
        axios.delete('https://602dd56696eaad00176dcd53.mockapi.io/api/v1/to-do/' + id, {
        })
            .then(function (response) {
                getTodo();
                console.log(response)
            })
            .catch(function (error) {
                console.log(error)
            });
    }

    const validateMessages = {
        required: `$\{label} is required!`
    };

    return (
        <>
            <Row>
                <Col span={12} style={{textAlign: 'center'}}>
                    <Card style={{width: '100%'}}>
                        <Form style={{color: 'red'}} onFinish={saveTodo} validateMessages={validateMessages}>
                            <Form.Item label="Task" name={'task'}
                                       rules={[
                                           {
                                               required: true,
                                           },
                                       ]}
                            >
                                <Input/>
                            </Form.Item>
                            <Form.Item label="Note" name={'note'}>
                                <TextArea
                                />
                            </Form.Item>
                            <Form.Item>
                                <Button htmlType="submit" style={{color: 'white', backgroundColor: '#1E857A'}}>Add
                                    Task</Button>
                            </Form.Item>
                        </Form>
                    </Card>
                </Col>
                <Col span={12}>
                    <TodoList item={todos} onDeleted={handleDelete}/>
                </Col>
            </Row>
        </>


    )
}

export default TodoForm;