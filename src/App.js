import 'antd/dist/antd.css'
import Headers from "./components/Headers/Headers";
import TodoForm from "./components/TodoForm/TodoForm";
import './App.css';

function App() {
    return (
        <>
            <Headers/>
            <TodoForm/>
        </>
    );
}

export default App;